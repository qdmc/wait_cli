/**
* Create by goland
* Author: 墨城
* Create Time: 2021-09-02 14:35
* Maintainers:
 */
package wait_cli

import (
	"bufio"
	"fmt"
	"gitee.com/qdmc/wait_cli/cmd"
	"gitee.com/qdmc/wait_cli/tools"
	"github.com/spf13/cobra"
	"os"
	"strings"
)

func Start() {
	runCmd := "wait_command"
	for {
		r := bufio.NewReader(os.Stdin)
		res, err := r.ReadString('\n')
		if err != nil {
			fmt.Println("read is error....", err)
			return
		}
		if strings.ToLower(tools.RemoveLBR(strings.Trim(res, " "))) == "exit" {
			os.Exit(0)
		}
		os.Args = []string{
			runCmd,
		}
		args := strings.Split(res, " ")
		Args := []string{}
		if len(args) >= 1 {
			for _, a := range args {
				arg := tools.RemoveLBR(strings.Trim(a, " "))
				if arg != "" && arg != runCmd {
					Args = append(Args, arg)
				}
			}
			if len(Args) > 0 {
				os.Args = append(os.Args, Args...)
			} else {
				os.Args = append(os.Args, "--help")
			}
		}
		cmd.Execute()
	}
}
func RegisterCmd(command *cobra.Command) {
	cmd.RootCmd.AddCommand(command)
}
