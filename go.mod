module gitee.com/qdmc/wait_cli

go 1.15

require (
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
)
