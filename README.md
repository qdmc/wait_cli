# wait_cli

#### 介绍
Stdin阻塞进程不退出,等待Stdin输出,cobra解析命令行

#### 使用环境
```
# 开启go mod
GO111MODULE=on
# 配置代理(中国,本地)
GOPROXY=https://goproxy.cn,direct
# gitee.com 不走代理
GONOPROXY=*.gitee.com
GONOSUMDB=*.gitee.com
GOPRIVATE=*.gitee.com
```

#### 引入
```
go get  gitee.com/qdmc/wait_cli
```

#### 使用
```
### 入口文件 /main.go ###
import (
	cli "gitee.com/qdmc/wait_cli"
	_ "waitCliTest/cmd"
)

func main()  {
	cli.Start()
}

### 你自己命令   /cmd/testCmd.go  ###
package cmd

import (
	"fmt"
	cli "gitee.com/qdmc/wait_cli"
	"github.com/spf13/cobra"
)

func init()  {
	cli.RegisterCmd(TestCmd)
}

var TestCmd = &cobra.Command{
	Use: "test",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print("\n就是一个测试命令\n")
	},
}

```

#### 使用场景
##### 项目初期开发
##### 性能测试
