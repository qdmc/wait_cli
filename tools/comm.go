/**
* Create by goland
* Author: 墨城
* Create Time: 2021-08-28 14:50
* Maintainers: 公用函数
 */
package tools

import "regexp"

// 去命令行多余的字符
func RemoveLBR(text string) string {
	re := regexp.MustCompile(`\x{000D}\x{000A}|[\x{000A}\x{000B}\x{000C}\x{000D}\x{0085}\x{2028}\x{2029}]`)
	return re.ReplaceAllString(text, ``)
}
