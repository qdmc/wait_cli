/**
* Create by goland
* Author: 墨城
* Create Time: 2021-09-01 10:21
* Maintainers:
 */
package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

func init() {
	RootCmd.AddCommand(exitCmd)
}

var exitCmd = &cobra.Command{
	Use:   "exit",
	Short: "退出",
	Long:  `退出阻塞,结束进程`,
	Run: func(cmd *cobra.Command, args []string) {
		os.Exit(0)
	},
}
